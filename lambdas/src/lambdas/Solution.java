package lambdas;
interface PerformOperation {
	 boolean check(int a);
	}
public class Solution {
	public PerformOperation isOdd(){
	    return (int a)->{
	    
	      return a%2 !=0;
	    
	};
	}
	public PerformOperation isPrime() {
	    return a->{
	        if (a<=1)
	            return false;
	        for(int i=2;i*i<=a;i++)
	        {
	           if (a%i ==0)
	               return false;
	        }
	        return true;
	    };
	}
	public PerformOperation isPalindrome() {
	    return a->{
	        String numString=Integer.toString(a);
	        int i=0; 
	        int j=numString.length()-1;
	        while (i<j) {
	            if (numString.charAt(i)!=numString.charAt(j))
	                return false;
	            i++;
	            j--;
	        }
	        return true;
	    };
	}
}
